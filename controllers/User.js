const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {
  return User.find({ email: reqBody.email }).then((result) => {
    if (result.length > 0) {
      return true;
    } else {
      return false;
    }
  });
};

// register
module.exports.registerUser = (reqBody) => {
  let newUser = new User({
    firstName: reqBody.firstName,
    lastName: reqBody.lastName,
    email: reqBody.email,
    mobileNo: reqBody.mobileNo,
    password: bcrypt.hashSync(reqBody.password, 10),
    /* 
    Syntax:
        bcrypt.hashSync(<stringToBeHashed>, <saltRounds>)
    */
  });
  return newUser.save().then((user, err) => {
    if (err) {
      return false;
    } else {
      return true;
    }
  });
};

// login
module.exports.loginUser = (reqBody) => {
  return User.findOne({ email: reqBody.email }).then((result) => {
    if (result == null) {
      return false;
    } else {
      const isPasswordCorrect = bcrypt.compareSync(
        reqBody.password,
        result.password
      );
      if (isPasswordCorrect) {
        return { access: auth.createAccessToken(result) };
      } else {
        return false;
      }
    }
  });
};

// module.exports.updateStatus = (userId, newStatus) => {
//   return User.findById(userId.id).then((result) => {
//     result.isAdmin = newStatus.isAdmin;
//     return result.save().then((updatedStatus, saveErr) => {
//       if (saveErr) {
//         console.log(saveErr);
//         return false;
//       } else {
//         return updatedStatus;
//       }
//     });
//   });
// };

// Activity S33

// get profile by ID
module.exports.getProfile = (userId) => {
  return User.findById(userId.id).then((result) => {
    result.password = "";
    return result;
  });
};

// for enrolling user

module.exports.enroll = async (data, user) => {
  if (user.isAdmin === false) {
    let isUserUpdated = await User.findById(data.userId).then((user) => {
      user.enrollments.push({ courseId: data.courseId });
      return user.save().then((user, err) => {
        if (err) {
          return false;
        } else {
          return true;
        }
      });
    });
    // using the "await" keyword will allow the enroll method to complete the course before returning a response back
    let isCourseUpdated = await Course.findById(data.courseId).then(
      (course) => {
        // adds the userId in the course's enrollees array
        course.enrollees.push({ userId: data.userId });
        // save the updated course information
        return course.save().then((course, err) => {
          if (err) {
            return false;
          } else {
            return true;
          }
        });
      }
    );
    // condition that will check if the user and course documents have been updated
    if (isUserUpdated && isCourseUpdated) {
      return true;
    } else {
      return false;
    }
  } else {
    return "Not Authorized";
  }
};
