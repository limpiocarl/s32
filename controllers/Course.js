const Course = require("../models/Course");

// Activity S34

module.exports.addCourse = async (reqBody, user) => {
  if (user.isAdmin === true) {
    let newCourse = new Course({
      name: reqBody.name,
      description: reqBody.description,
      price: reqBody.price,
    });
    return newCourse.save().then((course, err) => {
      if (err) {
        return false;
      } else {
        return course;
      }
    });
  } else {
    return "Not Authorized";
  }
};

// retrieve all courses
module.exports.getAllCourses = () => {
  return Course.find({}).then((result) => {
    return result;
  });
};

// retrieve all active courses
module.exports.getAllActive = () => {
  return Course.find({ isActive: true }).then((result) => {
    return result;
  });
};

// retrieve specific course
module.exports.getCourse = (reqParams) => {
  return Course.findById(reqParams.courseId).then((result) => {
    return result;
  });
};

// update a course
module.exports.updateCourse = (reqParams, reqBody) => {
  let updatedCourse = {
    name: reqBody.name,
    description: reqBody.description,
    price: reqBody.price,
  };
  return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then(
    (course, error) => {
      if (error) {
        return false;
      } else {
        return true;
      }
    }
  );
};

// update a course (2)
// module.exports.updateCourse = (reqParams, reqBody, user) => {
//   return Course.findByIdAndUpdate(reqParams.courseId).then((result) => {
//     if (user.isAdmin === true) {
//       result.name = reqBody.name;
//       result.description = reqBody.description;
//       result.price = reqBody.price;
//       return result.save().then((updatedCourse, err) => {
//         if (err) {
//           return false;
//         } else {
//           return updatedCourse;
//         }
//       });
//     } else {
//       return "Not authorized to update course";
//     }
//   });
// };

// Activity S35

// update isActive status
module.exports.updateStatus = (reqParams, reqBody, user) => {
  return Course.findByIdAndUpdate(reqParams.courseId).then((result) => {
    if (user.isAdmin === true) {
      result.isActive = reqBody.isActive;
      return result.save().then((updatedCourse, err) => {
        if (err) {
          return false;
        } else {
          return true;
        }
      });
    } else {
      return "Not authorized to update course";
    }
  });
};
