const express = require("express");
const router = express.Router();
const courseController = require("../controllers/Course");
const auth = require("../auth");

// Activity S34

router.post("/", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  console.log(userData);
  courseController
    .addCourse(req.body, userData)
    .then((resultFromController) => res.send(resultFromController));
});

module.exports = router;

// retrieve all the courses
router.get("/all", (req, res) => {
  courseController
    .getAllCourses()
    .then((resultFromController) => res.send(resultFromController));
});

// retrive all active courses
router.get("/active", (req, res) => {
  courseController
    .getAllActive()
    .then((resultFromController) => res.send(resultFromController));
});

// retrieve specific course
router.get("/:courseId", (req, res) => {
  courseController
    .getCourse(req.params)
    .then((resultFromController) => res.send(resultFromController));
});

router.put("/:courseId", auth.verify, (req, res) => {
  courseController
    .updateCourse(req.params, req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// update course (2)
// router.put("/:courseId/update", auth.verify, (req, res) => {
//   const userData = auth.decode(req.headers.authorization);
//   courseController
//     .updateCourse(req.params, req.body, userData)
//     .then((resultFromController) => res.send(resultFromController));
// });

// Activity S35

// route for updating isActive status
router.put("/:courseId/update", auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  courseController
    .updateStatus(req.params, req.body, userData)
    .then((resultFromController) => res.send(resultFromController));
});
